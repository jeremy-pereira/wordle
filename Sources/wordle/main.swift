//
//  main.swift
//  
//
//  Created by Jeremy Pereira on 08/02/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import ArgumentParser
import WordleKit

struct WordleParser: ParsableCommand
{
	@Argument(help: """
A script provides a list of included and excluded
letter rules for various letter positions. A rule
looks like this: p:i^e where p is the letter position
(1 - 5), i is a string of included letters and e is a
string of excluded letters. So 2:abc^def means at
position 2 the letter can be one of a, b, c but must
not be d, e, f. Obviously, for a single letter,
having both includes and excludes is meaningless but
see below.

If p: is omitted, the rule is global i.e. applies to
the whole word i.e. abc^def means the word must have
a, b and c in it somewhere but must not have d,e and
f anywhere. Includes and excludes can also be omitted.
^abc means incude any but exclude a, b and c. abc^
means include a, b and c but exclude none. In the
latter case, the ^ may be omitted.

Rules are separated by spaces. The following, for
example, will get you close to an answer for Wordle
235
    or^ntesclakpi 1:^n 2:^o 3:^t 4:o 5:r
""")
	var script: String

	@Flag(help: """
Sorts the list according to a weighting for each word
based on letter counts in the word list. The
algorithm uniques the letters, so from rarer we get
rare and then assigns a value which is the number of
times it appears in the wordlist. These values are
totalled to giva a score for the word which is used
to sort the list in order of ascending score.
""")
	var cheat: Bool = false
	func run() throws
	{
		let compilationResult = ScriptCompiler().compile(script: script)
		let transform: ScriptCompiler.Transform
		switch compilationResult
		{
		case .errored(let errors):
			for error in errors
			{
				print("rule \(error.position): \(error.message)")
			}
			return
		case .success(let t):
			transform = t
		}
		do
		{
			let wordle = Wordle()
			var possibilities = try transform(wordle.wordList)
			if cheat
			{
				wordle.weightedSort(list: &possibilities)
			}
			print("\(possibilities.joined(separator: "\n"))")
			print("Count of words left: \(possibilities.count)")
		}
		catch
		{
			print("\(error)")
		}

	}
}

WordleParser.main()
