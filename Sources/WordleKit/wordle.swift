
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/// Wordle solver
public struct Wordle
{
	public let wordList: [String]
	public let letterCounts: [Character : Int]

	/// Create a Wordle structure
	/// - Parameter wordListLocation: Location of the wordlist. If `nil`, uses
	///                               the words packaged in the bundle.
	/// - Throws: if the list cannot be read
	public init(wordListLocation: String?) throws
	{
		// First get the word list
		let fileUrl: URL
		if let fileLocation = wordListLocation
		{
			fileUrl = URL(fileURLWithPath: fileLocation)
		}
		else
		{
			guard let bundleUrl = Bundle.module.url(forResource: "words5", withExtension: "txt")
			else
			{
				fatalError("The default word list (words5.txt) is missing")
			}
			fileUrl = bundleUrl
		}
		let fileContent = try String(contentsOf: fileUrl, encoding: .utf8)
		let strings = fileContent.split(separator: "\n").map { String($0) }
		self.wordList = strings
		// Now do the letter counts
		self.letterCounts = strings.reduce(into: [Character : Int]())
		{
			partialResult, word in
			word.forEach
			{
				let existingCount = partialResult[$0, default: 0]
				partialResult[$0] = existingCount + 1
			}
		}
    }

	/// Sort a word list according to a score
	/// - Parameter list: The list to sort
	/// - Returns: the sorted list
	public func weightedSorting(list: [String]) -> [String]
	{
		return list.sorted()
		{
			v0, v1 in
			let v0Set = Set(v0)
			let v1Set = Set(v1)
			let v0Counts = v0Set.map { letterCounts[$0, default: 0] }
			let v1Counts = v1Set.map { letterCounts[$0, default: 0] }
			let v0Total = v0Counts.reduce(0, +)
			let v1Total = v1Counts.reduce(0, +)
			return v0Total < v1Total
		}
	}
	public func weightedSort(list: inout [String])
	{
		list.sort()
		{
			v0, v1 in
			let v0Set = Set(v0)
			let v1Set = Set(v1)
			let v0Counts = v0Set.map { letterCounts[$0, default: 0] }
			let v1Counts = v1Set.map { letterCounts[$0, default: 0] }
			let v0Total = v0Counts.reduce(0, +)
			let v1Total = v1Counts.reduce(0, +)
			return v0Total < v1Total
		}
	}

	/// Initialise from the bundle packaged list
	public init()
	{
		// If we can't read the bundle list, we already will have fatal error'd
		try! self.init(wordListLocation: nil)
	}
}

extension Wordle
{

	/// Errors associated with Wordle
	public enum Error: Swift.Error
	{
		/// Could not find or read the word list
		case noWordList
		/// includes and excludes contain commmon letters
		case includesExcludesOverlap
		/// A position index is out of bounds for a word size
		case wordBounds
	}
}


/// Protocol for something that can provide a match to a letter or word
public protocol WordleMatching: ExpressibleByStringLiteral
{
	/// Initialise from a collection of characters
	///
	/// - Parameter letters the collection of letters from which to construct the match
	init<S: Collection>(_ letters: S) where S.Element == Character

	/// Does this match a particular letter
	///
	/// - Parameter letter the letter to match
	/// - Returns: `true` if there's a match
	func match(letter: Character) -> Bool

	/// Does this match a set of letters
	///
	/// - Parameter letters the letters to match
	/// - Returns: `true` if there's a match
	func match<S: Collection>(letters: S) -> Bool where S.Element == Character

	/// Create a new match by combining this match  including the set of letters
	/// - Parameter letters letters the new match must include
	/// - Returns: A new match
	func include<S: Collection>(_ letters: S) throws -> Wordle.Match where S.Element == Character
	func exclude<S: Collection>(_ letters: S) throws -> Wordle.Match where S.Element == Character
}

extension WordleMatching
{
	public init(stringLiteral value: String)
	{
		self.init(value)
	}
}

extension Wordle
{

	/// Wordle match that must match *all* of our letters
	///
	/// If our match letter set is empty, we assume everything matches. If not
	/// everything must match our letters. A sinle letter will only match ifd our
	/// letter set has only one letter in it.
	public struct Include: WordleMatching
	{

		/// Signle letter match
		///
		/// This will only match if our letters contains one letter and it is
		/// the same letter.
		/// - Parameter letter: Letter to match
		/// - Returns: `true` if there is a match
		public func match(letter: Character) -> Bool
		{
			return match(letters: [letter])
		}

		public func match<S: Collection>(letters: S) -> Bool where S.Element == Character
		{
			guard !self.letters.isEmpty else { return true }
			let intersection = self.letters.intersection(Set(letters))
			let matchRet = intersection.count == self.letters.count
			return matchRet
		}

		let letters: Set<Character>

		public init<S: Collection>(_ letters: S) where S.Element == Character
		{
			self.letters = Set(letters)
		}

		public static let any = Include("")

		public func include<S: Collection>(_ letters: S) -> Wordle.Match where S.Element == Character
		{
			let union = self.letters.union(letters)
			return Wordle.Match(include: Include(union))
		}

		public func exclude<S: Collection>(_ letters: S) throws -> Wordle.Match where S.Element == Character
		{
			return try Wordle.Match(include: self, exclude: Exclude(letters))
		}

	}

	public struct Exclude: WordleMatching
	{
		public func match(letter: Character) -> Bool
		{
			return !letters.contains(letter)
		}

		public func match<S: Collection>(letters: S) -> Bool where S.Element == Character
		{
			return self.letters.intersection(Set(letters)).isEmpty
		}

		let letters: Set<Character>

		public 	init<S: Collection>(_ letters: S) where S.Element == Character
		{
			self.letters = Set(letters)
		}


		public static let none = Exclude("")


		public func include<S: Collection>(_ letters: S) throws -> Wordle.Match where S.Element == Character
		{
			return try Wordle.Match(include: Include(letters), exclude: self)
		}

		public func exclude<S: Collection>(_ letters: S) -> Wordle.Match where S.Element == Character
		{
			let union = self.letters.union(letters)
			return Wordle.Match(exclude: Exclude(union))
		}
	}

	public struct Match: WordleMatching
	{
		public func match(letter: Character) -> Bool
		{
			let includeOK = include.match(letter: letter)
			let excludeOK = exclude.match(letter: letter)
			return includeOK && excludeOK
		}

		public func match<S: Collection>(letters: S) -> Bool where S.Element == Character
		{
			let includeOK = include.match(letters: letters)
			let excludeOK = exclude.match(letters: letters)
			return includeOK && excludeOK
		}

		let include: Include
		let exclude: Exclude

		public 	init<S: Collection>(_ letters: S) where S.Element == Character
		{
			self.init(include: Include(letters))
		}

		public init(include: Include)
		{
			self.include = include
			self.exclude = Exclude.none
		}

		public init(exclude: Exclude)
		{
			self.include = Include.any
			self.exclude = exclude
		}

		public init()
		{
			self.include = Include.any
			self.exclude = Exclude.none
		}

		public init(include: Include, exclude: Exclude) throws
		{
			guard include.letters.intersection(exclude.letters).isEmpty
			else { throw Wordle.Error.includesExcludesOverlap }
			self.include = include
			self.exclude = exclude
		}

		public func include<S: Collection>(_ letters: S) throws -> Wordle.Match where S.Element == Character
		{
			let union = self.include.letters.union(letters)
			return try Wordle.Match(include: Include(union), exclude: self.exclude)
		}

		public func exclude<S: Collection>(_ letters: S) throws -> Wordle.Match where S.Element == Character
		{
			let union = self.exclude.letters.union(letters)
			return try Wordle.Match(include: self.include, exclude: Exclude(union))
		}
	}
}
