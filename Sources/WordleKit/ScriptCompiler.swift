//
//  ScriptCompiler.swift
//  
//
//  Created by Jeremy Pereira on 09/02/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Toolbox

/// Compiler to compile scripts into transsforms to filter words from the
/// word list
public struct ScriptCompiler
{
	public init() {}

	/// Compile a script into a function that will filter a word list
	/// - Parameter script: The script to compile
	/// - Returns: A result that is either a list of errors or a function to
	///            apply to the word list.
	public func compile(script: String) -> CompilerResult
	{
		let ruleStrings = script.split(separator: " ", omittingEmptySubsequences: true)
		let compilations = ruleStrings.enumerated().map { compile(rule: $1, position: $0) }
		let errors = compilations.flatMap { $0.errors }
		guard errors.isEmpty else
		{
			return CompilerResult.errored(errors)
		}
		let transforms = compilations.compactMap { $0.transform }
		// Use an optional transform so we don't always have to start with an
		// identity function
		let composedTransforms = transforms.reduce(nil)
		{
			(partialResult: Transform?, nextTransform: @escaping Transform) -> Transform? in
			if let partialResult = partialResult
			{
				return { (s: [String]) in try nextTransform(partialResult(s)) }
			}
			else
			{
				return nextTransform
			}
		}
		return CompilerResult.success(composedTransforms ?? { $0 })
	}

	private func compile<S: Collection>(rule: S, position: Int) -> CompilerResult
	where S.Element == Character
	{
		// Split at the colon to see if we have one
		let colonSplit = rule.split(separator: ":")
		let positionString: String?
		let includeExcludeString: String
		switch colonSplit.count
		{
		case 1:
			positionString = nil
			includeExcludeString = String(colonSplit[0])
		case 2:
			if colonSplit[0].isEmpty
			{
				positionString = nil
			}
			else
			{
				positionString = String(colonSplit[0])
			}
			includeExcludeString = String(colonSplit[1])
		default:
			let error = Error(position: position, message: "Invalid number of colons")
			return CompilerResult.errored([error])
		}
		// Now split the includeExclude at the ^
		let ieSplit = includeExcludeString.split(separator: "^", omittingEmptySubsequences: false)
		let includeString: String
		let excludeString: String
		switch ieSplit.count
		{
		case 1:
			includeString = String(ieSplit[0])
			excludeString = ""
		case 2:
			includeString = String(ieSplit[0])
			excludeString = String(ieSplit[1])
		default:
			let error = Error(position: position, message: "Too many include/exclude parts")
			return CompilerResult.errored([error])
		}
		// First construct a match
		let include = !includeString.isEmpty ? Wordle.Include(includeString) : Wordle.Include.any
		let exclude = !excludeString.isEmpty ? Wordle.Exclude(excludeString) : Wordle.Exclude.none
		guard let match = try? Wordle.Match(include: include, exclude: exclude)
		else
		{
			let error = Error(position: position, message: "include and exclude overlap")
			return CompilerResult.errored([error])
		}

		let transform: Transform
		if let positionString = positionString
		{
			guard let letterPosition = Int(positionString)
			else
			{
				let error = Error(position: position, message: "position is not a number")
				return CompilerResult.errored([error])
			}
			guard (1 ... 5).contains(letterPosition)
			else
			{
				let error = Error(position: position, message: "position is not between 1 and 5")
				return CompilerResult.errored([error])
			}
			transform = { try $0.wordle(match: match, position: letterPosition - 1) }
		}
		else
		{
			transform = { $0.wordle(match: match) }
		}
		return CompilerResult.success(transform)
	}
}

extension ScriptCompiler
{
	/// Type of functions that transform the word list
	public typealias Transform = ([String]) throws -> [String]
	/// Compiler error
	public struct Error: Swift.Error
	{
		/// The position of the rule in the rule string
		public let position: Int
		/// The error message
		public let message: String

		init(position: Int, message: String)
		{
			self.position = position
			self.message = message
		}
	}

	/// The result of compiling a script
	public enum CompilerResult
	{
		/// Compilation ended in error
		case errored([Error])
		/// Compilation succeeded
		case success(Transform)

		/// All the compilation errors
		///
		/// 
		public var errors: [Error]
		{
			switch self
			{
			case .errored(let errorList):
				return errorList
			case .success(_):
				return []
			}
		}
		public var transform: Transform?
		{
			switch self
			{
			case .errored(_):
				return nil
			case .success(let transform):
				return transform
			}
		}
	}
}
