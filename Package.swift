// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "wordle",
	platforms: [
		.macOS(.v12),
	],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "WordleKit",
            targets: ["WordleKit"]),
		.executable(name: "wordle", targets: ["wordle"]),

    ],
    dependencies: [
		.package(name: "Toolbox", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/Toolbox.git", from: "21.0.0"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.0.3"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "WordleKit",
            dependencies: ["Toolbox"],
			resources: [.copy("Resources/words5.txt")]),
		.executableTarget(
			name: "wordle",
			dependencies: ["WordleKit",
						   .product(name: "Toolbox", package: "Toolbox"),
						   .product(name: "ArgumentParser", package: "swift-argument-parser")]),

        .testTarget(
            name: "wordleTests",
            dependencies: ["WordleKit"]),
    ]
)
