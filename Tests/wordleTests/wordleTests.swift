import XCTest
@testable import WordleKit

final class wordleTests: XCTestCase
{

	func testGetDefaultStringd()
	{
		let strings = Wordle().wordList
		XCTAssert(strings.count > 0)
		XCTAssert(strings.contains("frame"))
		XCTAssert(strings.contains("notes"))
		XCTAssert(strings.contains("acrid"))
	}

	func testIncludeExclude()
	{
		let strings = Wordle().wordList
		do
		{
			let possibilities = try strings.wordle(match: Wordle.Match(include: "era", exclude: "notscid"))
			XCTAssert(possibilities.contains("frame"))
			XCTAssert(!possibilities.contains("notes"))
			XCTAssert(!possibilities.contains("acrid"))
			print("Count of words left: \(possibilities.count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testOverlap()
	{
		do
		{
			let strings = ["humor"]
			let _ = try strings.wordle(match: Wordle.Match(include: "era", exclude: "notrscid"))
			XCTFail("Should throw")
		}
		catch
		{
			// Should fail
		}
	}

	func testWOR1Bug()
	{
		let compilationResult = ScriptCompiler().compile(script: "oe")
		let transform: ScriptCompiler.Transform
		switch compilationResult
		{
		case .errored(let errors):
			for error in errors
			{
				XCTFail("rule \(error.position): \(error.message)")
			}
			return
		case .success(let t):
			transform = t
		}
		do
		{
			let possibilities = try transform(["ology"])
			XCTAssert(possibilities.isEmpty, "Found \(possibilities.count) words: \(possibilities.joined(separator: ","))")
		}
		catch
		{
			XCTFail("\(error)")
		}

	}

}
